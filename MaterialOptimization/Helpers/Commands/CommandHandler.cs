﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MaterialOptimization.Helpers.Commands
{
    public class CommandHandler : ICommand
    {
        private Action _action;
        private bool _canExecute;
        public object Parameter { get; private set; }
        public CommandHandler(bool canExecute)
        {
            _canExecute = canExecute;
        }

        public CommandHandler(Action action, bool canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            Parameter = parameter;
            _action();
        }

        public Action Func
        {
            set
            {
                _action = value;
            }
        }
    }
}
