﻿using MaterialOptimization.Helpers.Commands;
using MaterialOptimization.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MaterialOptimization.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddSectionCommand
        {
            get
            {
                CommandHandler command = new CommandHandler(true);
                command.Func = () => AddSectionToObservable(command.Parameter);
                return command;
            }
        }
        public ICommand RemoveSectionCommand
        {
            get
            {
                CommandHandler command = new CommandHandler(true);
                command.Func = () => RemoveSectionFromObservable(command.Parameter);
                return command;
            }
        }

        public ICommand CalculateCommand
        {
            get
            {
                CommandHandler command = new CommandHandler(true);
                command.Func = () => Calculate(command.Parameter);
                return command;
            }
        }

        public ObservableCollection<BinObservable> Bins
        {
            get
            {
                return _bins;
            }
            set
            {
                if (_bins != value)
                {
                    _bins = value;
                    RaisePropertyChanged("Bins");
                }
            }
        }
        public ObservableCollection<float> InputData
        {
            get
            {
                return _inputData;
            }
            set
            {
                if (_inputData != value)
                {
                    _inputData = value;
                    RaisePropertyChanged("InputData");
                }
            }
        }
        public string InputField
        {
            get
            {
                return _inputField;
            }
            set
            {
                if (_inputField != value)
                {
                    _inputField = value;
                    RaisePropertyChanged("InputField");
                }
            }
        }
        public float MaxSectionLength
        {
            get { return _maxSectionLength; }
            set
            {
                if (_maxSectionLength != value)
                {
                    _maxSectionLength = value;
                    RaisePropertyChanged("MaxSectionLength");
                }
            }
        }
        public float HalfSectionLength
        {
            get { return _halfSectionLength; }
            set
            {
                if (_halfSectionLength != value)
                {
                    _halfSectionLength = value;
                    RaisePropertyChanged("HalfSectionLength");
                }
            }
        }

        public string ResultSummary
        {
            get { return _resultSummary; }
            set
            {
                if (_resultSummary != value)
                {
                    _resultSummary = value;
                    RaisePropertyChanged("ResultSummary");
                }
            }
        }

        public int? CurrentlySelectedIndex
        {
            get
            {
                return _currentlySelectedIndex;
            }
            set
            {

                _currentlySelectedIndex = value;
                RaisePropertyChanged("CurrentlySelected");
            }
        }

        private OptimizationCalculator _optimizationCalculator;

        private string _inputField;
        private string _resultSummary;
        private float _maxSectionLength;
        private float _halfSectionLength;
        private int? _currentlySelectedIndex;
        private ObservableCollection<float> _inputData = new ObservableCollection<float>();
        private ObservableCollection<BinObservable> _bins = new ObservableCollection<BinObservable>();

        public void RaisePropertyChanged(string _propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(_propName));
        }

        private void AddSectionToObservable(object parameter)
        {
            string[] sections = _inputField.Split(',');
            foreach (string s in sections)
            {
                float stringValue;
                bool success = float.TryParse(s, out stringValue);
                if (success)
                {
                    InputData.Add(stringValue);
                }
            }

        }
        private void RemoveSectionFromObservable(object parameter)
        {
            if (CurrentlySelectedIndex != null && CurrentlySelectedIndex < InputData.Count && InputData.Count>0)
            {
                int index =(int)CurrentlySelectedIndex;
                InputData.RemoveAt(index);
                CurrentlySelectedIndex = null;
            }
        }

        private void Calculate(object parameter)
        {
            if (AreInputFieldsValid() == false)
            {
                MessageBox.Show("Input is not valid. Enter proper values.");
                return;
            }

            _optimizationCalculator = new OptimizationCalculator(InputData.ToList(), MaxSectionLength, HalfSectionLength);

            List<Bin> optimizedBins = _optimizationCalculator.GetOptimized();
            Bins = new ObservableCollection<BinObservable>();

            foreach (Bin bin in optimizedBins)
            {
                BinObservable binObservable = new BinObservable();

                binObservable.binCurrentSum = bin.binCurrentSum;
                binObservable.binMax = bin.binMax;
                binObservable.floatsInBin = new ObservableCollection<float>();
                binObservable.wasteSum = bin.wasteSum;
                foreach (float floatValue in bin.floatsInBin)
                {
                    binObservable.floatsInBin.Add(floatValue);
                }
                Bins.Add(binObservable);
            }

            (float usedMaterials, float wastedMaterials) usedAndWastedSummary = _optimizationCalculator.GetMaterialsSummaryTuple(optimizedBins);

            ResultSummary = string.Format("Used materials summary: {0}.\nWasted materials summmary: {1}.", usedAndWastedSummary.usedMaterials, usedAndWastedSummary.wastedMaterials);
        }

        private bool AreInputFieldsValid()
        {
            return MaxSectionLength > 0 && HalfSectionLength > 0 && (MaxSectionLength / 2 == HalfSectionLength) && InputData.Any() && InputData.Max() <= MaxSectionLength;
        }
    }
}
