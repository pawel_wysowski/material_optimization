﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialOptimization.Models
{
    public class Bin
    {
        public List<float> floatsInBin { get; set; }
        public float binCurrentSum { get; set; }
        public float binMax { get; set; }
        public float wasteSum { get; set; }
    }
}
