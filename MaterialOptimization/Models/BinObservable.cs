﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialOptimization.Models
{
    public class BinObservable
    {
        public ObservableCollection<float> floatsInBin { get; set; }
        public float binCurrentSum { get; set; }
        public float binMax { get; set; }
        public float wasteSum { get; set; }
    }
}
