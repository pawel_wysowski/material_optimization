﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialOptimization.Models
{
    public class OptimizationCalculator
    {
        private List<float> _inputSections = new List<float>();
        private float _max;
        private float _min;
        public OptimizationCalculator(List<float> inputSections, float max, float min)
        {
            _max = max;
            _min = min;
            _inputSections = inputSections;
            _inputSections.Sort((floatA, floatB) => floatB.CompareTo(floatA));
        }

        public List<Bin> GetOptimized()
        {
            float sum = 0;
            foreach (float f in _inputSections)
            {
                sum += f;
            }


            float temp = sum / _max;

            int numberOfMaxBins = (int)temp;
            int numberOfHalfBins = 0;
            float tempB = sum - numberOfMaxBins * _max;

            if(tempB % _min != 0 && _inputSections.Min(f => f > _min))
            {
                numberOfMaxBins += 1;
            }
            else
            {
                numberOfHalfBins = CalculateMinBinsNumber(tempB);
            }
            

            List<Bin> bins = new List<Bin>();


            for (int i = 0; i < numberOfMaxBins; i++)
            {
                Bin bin = new Bin();
                bin.binMax = _max;
                bin.binCurrentSum = 0;
                bin.floatsInBin = new List<float>();
                bins.Add(bin);
            }

            for (int i = 0; i < numberOfHalfBins; i++)
            {
                Bin bin = new Bin();
                bin.binMax = _min;
                bin.binCurrentSum = 0;
                bin.floatsInBin = new List<float>();
                bins.Add(bin);
            }

            foreach (float f in _inputSections)
            {
                foreach (Bin b in bins)
                {
                    if (b.binCurrentSum + f <= b.binMax)
                    {
                        b.binCurrentSum += f;
                        b.floatsInBin.Add(f);
                        break;
                    }
                }
            }


            foreach (Bin b in bins)
            {
                b.wasteSum = b.binMax - b.binCurrentSum;
            }


            return bins;
        }

        private int CalculateMinBinsNumber(float temp)
        {
            float tempResult = temp / _min;

            if (tempResult % _min == 0)
            {
                return (int)tempResult;
            }
            else
            {
                return (int)Math.Ceiling((double)Math.Abs(tempResult));
            }
        }

        public (float, float) GetMaterialsSummaryTuple(List<Bin> bins)
        {
            (float usedMaterials, float wastedMaterials) summaryTuple;
            summaryTuple.usedMaterials = bins.Sum(b => b.binCurrentSum);
            summaryTuple.wastedMaterials = bins.Sum(b => b.wasteSum);
            return summaryTuple;
        }
    }
}
